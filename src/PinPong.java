public class PinPong extends Thread {
    public final static Process process = new Process();

    public class PinPongThread extends Thread {
        private Process.Action action;

        public PinPongThread(Process.Action action) {
            this.action = action;
        }

        public void run() {
            try {
                for (; ; ) {
                    synchronized (PinPong.process) {
                        if(PinPong.process.getProcessAction() != action) {
                            PinPong.process.action(this.getName(), action);
                            PinPong.process.notify();
                        }
                        PinPong.process.wait();
//                Thread.sleep((long) (1500 * (1 - Math.random())));
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    PinPong() {
        PinPongThread pin = new PinPongThread(Process.Action.PIN);
        pin.start();
        PinPongThread pong = new PinPongThread(Process.Action.PONG);
        pong.start();
    }

    public static void main(String[] args) {
        PinPong pinPong = new PinPong();
//        pinPong.start();
    }
}
