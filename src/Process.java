public class Process {

    public enum Action {
        PIN("pin"), PONG("pong");

        private String name;

        Action(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public Action geInvert() {
            return name.equals(PIN.name) ? PONG : PIN;
        }
    }

    private Action processAction;
    private String lastActionFrom;

    public void action(String threadName, Action action) {
        lastActionFrom = threadName;
        processAction = action;
        display();
    }

    public Action getProcessAction() {
        return processAction;
    }

    public void display() {
        System.out.println("From: " + lastActionFrom + ", action: " + processAction.name);
    }
}
